import { LitElement, html } from 'lit-element'

class TestApi extends LitElement{

    static get properties() {
        return {
            films: {type: Array}
        }
    }

    constructor() {
        super()

        this.films = []
        this.getMovieData()
    }

    render(){
        return html`
            ${this.films.map(
                film => html`
                    <div>
                        La película ${film.title} del director ${film.director}
                    </div>
                ` 
            )}
        `; 
    }

    getMovieData() {
        console.log("*** get movie data")

        let xhr = new XMLHttpRequest()

        xhr.onload = () => {
            if(xhr.status === 200)
            {
                console.log("Ha dado 200")

                let response = xhr.responseText

                console.log("Resultado " + JSON.parse(response).results)

                this.films = JSON.parse(response).results
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films/")
        xhr.send()

        console.log("Fin de getmoviedata")
    }



}

customElements.define('test-api',TestApi)