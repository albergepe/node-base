import { LitElement, html} from 'lit-element'
import "../persona-ficha-listado/persona-ficha-listado.js"
import "../persona-form/persona-form.js"

class PersonaMain extends LitElement{

    static get properties(){
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        }
    }


    constructor(){
        super()
        this.showPersonForm = false;
        this.people = [
            {
                name: "Alberto",
                yearsInCompany: 2,
                profile: "Alberto ipsum dolor sit amet",
                photo: {
                    src: "./img/tous-paco-image.jpg",
                    alt: "paco-alber"
                }
            },
            {
                name: "Emma",
                yearsInCompany: 5,
                profile: "Emma ipsum dolor sit amet",
                photo: {
                    src: "./img/tous-paco-image.jpg",
                    alt: "paco-emma"
                }
            },
            {
                name: "Ana",
                yearsInCompany: 8,
                profile: "Ana ipsum dolor sit amet",
                photo: {
                    src: "./img/tous-paco-image.jpg",
                    alt: "paco-ana"
                }
            },
            {
                name: "Luis",
                yearsInCompany: 10,
                profile: "Luis ipsum dolor sit amet",
                photo: {
                    src: "./img/tous-paco-image.jpg",
                    alt: "paco-luis"
                }
            },
            {
                name: "Rubén",
                yearsInCompany: 20,
                profile: "Rubén ipsum dolor sit amet",
                photo: {
                    src: "./img/tous-paco-image.jpg",
                    alt: "paco-ruben"
                }
            }      
        ]
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        
            <h2 class="text-center">MAIN Personas</h2>
            <div class=row" id="peoplelist">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado fname="${person.name}" 
                        yearsInCompany="${person.yearsInCompany}"
                        .photo="${person.photo}"
                        profile="${person.profile}"
                        @delete-person="${this.deletePersonReceiver}"
                        @info-person="${this.moreInfoPerson}">
                        </persona-ficha-listado>` 
                )} 
                </div>
            </div>
            <div class="row">
                <persona-form @go-back="${this.goBack}" 
                 @persona-form-store="${this.personFormStore}" id="personForm" 
                 class="d-none border rounded border-primary" visible="${this.showPersonForm}">
                 </persona-form>
            </div>
        `; 
    }

    moreInfoPerson(e) {
        console.log("**moreInfoPerson")

        console.log("**moreInfoPerson" + e.detail.name)

        let choosenPerson = this.people.filter(
            person => person.name === e.detail.name
        )

        let personShow = {}
        
        personShow.name = choosenPerson[0].name
        personShow.profile = choosenPerson[0].profile
        personShow.yearsInCompany = choosenPerson[0].yearsInCompany

        
        this.shadowRoot.getElementById("personForm").person = personShow
        this.shadowRoot.getElementById("personForm").isUpdate = true
        this.showPersonForm = true

        
    }

    personFormStore(e) {
        console.log("**** person form store ¡guardamos")

        console.log("**** person form store ¡guardamos " + e.detail.person.name)
        console.log("**** person form store ¡guardamos " + e.detail.person.profile)
        console.log("**** person form store ¡guardamos " + e.detail.person.yearsInCompany)
        

        if (e.detail.isUpdate === false) {
            console.log("Update false, nueva persona")
            //this.people.push(e.detail.person)
            this.people = [...this.people, e.detail.person]
        }
        else {
            console.log("Update true, editar persona " + e.detail.person.name)

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            )
        }
        this.showPersonForm = false

    }

    updated(changedProperties) {
        console.log("updated en persona-main")

        if(changedProperties.has("showPersonForm")){
            console.log("el valor es " + this.showPersonForm)
    
                this.showPersonFormData();
        }

        if (changedProperties.has("people")){
            console.log("******* ha cmbiado people en persona main")

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people" , {
                        "detail": {
                            people: this.people
                        }
                    }
                )
            )
        } 
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        if(this.showPersonForm === true)
        {
            this.shadowRoot.getElementById("personForm").classList.remove("d-none")
            this.shadowRoot.getElementById("peoplelist").classList.add("d-none")
        }
        else
        {
            this.shadowRoot.getElementById("personForm").classList.add("d-none")
            this.shadowRoot.getElementById("peoplelist").classList.remove("d-none")
        }
    }



    deletePersonReceiver(e) {
        console.log("**deletePersonReceiver")

        console.log("**se borra la persona" + e.detail.name)

        this.people = this.people.filter(
            person => person.name != e.detail.name
        )
    }  
    
    goBack() {
        console.log("goBack del main")
        this.showPersonForm = false
    }
}

customElements.define('persona-main',PersonaMain)