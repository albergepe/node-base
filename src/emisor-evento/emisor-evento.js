import { LitElement, html } from 'lit-element'

class EmisorEvento extends LitElement{

    static get properties(){
        return {

        }
    }

    constructor(){
        super()
    }

    render(){
        return html`
            <h3>Emisor evento</h3> 
            <button @click="${this.sendEvent}">No pulsa</button>
        `; 
    }

    sendEvent(e) {
        console.log("*** pulsado el botón")
        console.log(e)

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    "detail" : {
                        "course": "techU",
                        "year": 2021
                    }
                }    
            )
        )
    }
}

customElements.define('emisor-evento',EmisorEvento)