import { LitElement, html } from 'lit-element'

class PersonaForm extends LitElement{

    static get properties(){
        return {
            person: {type: Object},
            isUpdate: {type: Boolean}
        }
    }

    constructor(){
        super()

        this.resetForm()
        this.isUpdate = false
        
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" 
                        ?disabled="${this.isUpdate}"
                        class="form-control" placeholder="nombre" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea class="form-control" @input="${this.updateProfile}" .value="${this.person.profile}" placeholder="perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text"  @input="${this.updateYearsInCompany}" class="form-control" 
                        .value="${this.person.yearsInCompany}" placeholder="Años en la empresa" />
                    </div>
                    <button class="btn btn-secondary" @click="${this.goBack}"><strong>Atrás</strong></button>
                    <button class="btn btn-success" @click="${this.savePerson}"><strong>Guardar</strong></button>
                </form>
            </div>
        `; 
    }

    resetForm() {
        console.log("Reset form data")
        this.person = {}
        this.person.profile = ""
        this.person.name = ""
        this.person.yearsInCompany = ""

        this.isUpdate = false
    }

    updateName(e) {
        console.log("****** update name " + e.target.value)
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("****** update profile " + e.target.value)
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("****** update yearsInCompany " + e.target.value)
        this.person.yearsInCompany = e.target.value;
    }

    goBack(e) {
        console.log("Atraaaaaas")

        this.resetForm();

       e.preventDefault();

      

       this.dispatchEvent(
           new CustomEvent("go-back",{})
       )
    }

    savePerson(e) {
        console.log("+++++savePerson")
        e.preventDefault()

        this.person.photo = {
            "src" : "./img/tous-paco-image.jpg",
            "alt" : "Persona-" + this.person.name
        }

        console.log("+++++savePerson name " + this.person.name)
        console.log("+++++savePerson profile " + this.person.profile)
        console.log("+++++savePerson yicom " + this.person.yearsInCompany)

        this.dispatchEvent(new CustomEvent("persona-form-store",
            {
                detail:
                {
                    person: {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    isUpdate: this.isUpdate
                }
            }    
        ))




    }
}

customElements.define('persona-form', PersonaForm)