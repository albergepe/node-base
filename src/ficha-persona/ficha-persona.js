import { LitElement, html } from 'lit-element'

class FichaPersona extends LitElement{

    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo : {type: String},
            photo: {type: Object}
        };
    }

    constructor(){
        super()

        this.name = "Prueba nombre"
        this.yearsInCompany = 2
        this.updatePersonInfo()
    }
    //PLANTILLA : Presenta el contenido (la vista de MVC)
    render(){
        return html`
        <div>
            <label>Nombre completo</label> 
            <input type="text" id="fpname" value="${this.name}" @input="${this.updateName}"></input>
            <br />
            <label>Años en la empresa</label> 
            <input type="text" id="fpyears" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}"></input>
            <br />
            <label>Foto</label> 
            <input type="text" id="fpphoto"></input>
            <br />
            <input type="text" value="${this.personInfo}" id="fppinfo" disabled></input>
        </div>
        `; 
    }

    updated(changedProperties) {
        console.log("***updated litE")
       /*changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue)
        });*/

       if (changedProperties.has("yearsInCompany")){
           this.updatePersonInfo();
       }
    }

    updateName(e){
        console.log("***update name")
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("*** updateYearsInCompany")
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo(){
        console.log("***update person info");
        console.log("years in company " + this.yearsInCompany)

      

        if(this.yearsInCompany >= 7)
        {
            this.personInfo = "lead";
        }
        else if (this.yearsInCompany >=5)
        {
            this.personInfo = "senior";
        }
        else if (this.yearsInCompany >=3)
        {
            this.personInfo = "team";
        }
        else
        {
            this.personInfo = "junior";
        }
        
    }
}

customElements.define('ficha-persona',FichaPersona)